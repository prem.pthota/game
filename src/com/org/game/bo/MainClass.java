package com.org.game.bo;

import java.util.Scanner;

import com.org.game.domain.Playar;
import com.org.game.domain.Skill;

public class MainClass {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter number of players");
		Integer size = scanner.nextInt();

		Playar[] playars = new Playar[size];
		for (int i = 0; i < size; i++) {
			int x = i + 1;
			System.out.println("Enter Player " + x + " details : ");
			System.out.println("Enter Player Name : ");
			Playar playar = new Playar();
			playar.setName(scanner.nextLine());
			System.out.println("Enter Country Name : ");
			playar.setCountry(scanner.nextLine());
			System.out.println("Enter Skill :");
			Skill skill = new Skill();
			skill.setSkillName(scanner.nextLine());
			playar.setSkill(skill);
			playars[i] = playar;
		}
		PlayarBO playarBO = new PlayarBOImpl();
		playarBO.viewDetails(playars);
		System.out.println("Enter Skill");
		playarBO.printPlayerDetailsWithSkill(playars, scanner.nextLine());
	}
}
