package com.org.game.bo;

import com.org.game.domain.Playar;

public interface PlayarBO {

	void viewDetails(Playar[] playerList);

	void printPlayerDetailsWithSkill(Playar[] playerList, String skill);

}
