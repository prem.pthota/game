package com.org.game.bo;

import java.util.ArrayList;
import java.util.List;

import com.org.game.domain.Playar;

public class PlayarBOImpl implements PlayarBO {

	@Override
	public void viewDetails(Playar[] playerList) {
		for (Playar playar : playerList) {
			System.out.println(playar.toString());
		}
	}

	@Override
	public void printPlayerDetailsWithSkill(Playar[] playerList, String skill) {
		List<Playar> list = new ArrayList<>();
		for (Playar playar : playerList) {
			if (playar.getSkill().getSkillName().equalsIgnoreCase(skill)) {
				list.add(playar);
			}
		}
		if (list.size() != 0) {
			System.out.println(list);
		} else {
			System.out.println("Skill not found");
		}
	}

}
