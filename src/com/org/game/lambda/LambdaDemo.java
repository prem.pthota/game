package com.org.game.lambda;

public class LambdaDemo {

	public static void main(String[] args) {
		IMyFunc myFactIMyFunc = (num) -> {
			int fact = 1;
			for (int i = 1; i <= num; i++) {
				fact = i * fact;
			}
			return fact;
		};
		System.out.println("Factorial of 7 : " + myFactIMyFunc.getValue(7));
	}

}
